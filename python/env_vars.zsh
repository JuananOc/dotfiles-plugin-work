# Locale. This should be specified because the pipenv installation does not write in MacOS the Pipfile.lock.
# This variables are required to use the pipenv without problem in macOS
export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8

# Add variables for pipenv to ignore the virtualenv created
export PIPENV_IGNORE_VIRTUALENVS=1
