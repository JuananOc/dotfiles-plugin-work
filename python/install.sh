#!/bin/bash

set -euo pipefail
IFS=$'\n\t'

blue "[Python] upgrade pip"
python3 -m pip install --upgrade pip --break-system-packages

blue "[Python] Install pipenv"
pip3 install pipenv --break-system-packages
green "[Python] Pipenv installed"

blue "[Python] Install aws-mfa-tools"
pip3 install aws-mfa-tools --break-system-packages
green "[Python] aws-mfa-tools installed"
