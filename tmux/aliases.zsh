#!/usr/bin/env bash

alias t="tmux-sessionizer"
alias tks='tmux kill-session -t'
alias tl='tmux ls'
alias ta='tmux attach -t'
