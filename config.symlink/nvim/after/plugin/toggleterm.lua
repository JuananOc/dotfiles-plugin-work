require("toggleterm").setup{
  hide_numbers = true, -- hide the number column in toggleterm buffers
  autochdir = true, -- when neovim changes it current directory the terminal will change it's own when next it's opened
  shade_terminals = true, -- NOTE: this option takes priority over highlights specified so if you specify Normal highlights you should set this to false
  start_in_insert = true,
  insert_mappings = true, -- whether or not the open mapping applies in insert mode
  terminal_mappings = true, -- whether or not the open mapping applies in the opened terminals
  persist_size = true,
  persist_mode = true, -- if set to true (default) the previous terminal mode will be remembered
  direction = 'float',
  close_on_exit = true, -- close the terminal window when the process exits
  shell = vim.o.shell, -- change the default shell
  auto_scroll = tre, -- automatically scroll to the bottom on terminal output
}

function _G.set_terminal_keymaps()
  local opts = {buffer = 0}
  vim.keymap.set('t', '<esc>', [[<C-\><C-n>]], opts)        -- <esc> to quit the terminal mode
  vim.keymap.set('t', 'jk', [[<C-\><C-n>]], opts)           -- jk to quit the terminal mode
--  vim.keymap.set('t', '<C-w>', [[<C-\><C-n><C-w>]], opts) -- C-w to select panes
end

-- if you only want these mappings for toggle term use term://*toggleterm#* instead
vim.cmd('autocmd! TermOpen term://* lua set_terminal_keymaps()')
