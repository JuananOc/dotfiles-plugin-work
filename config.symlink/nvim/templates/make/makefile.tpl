.DEFAULT_GOAL := help

.PHONY: help
help: Makefile
	@printf "\nChoose a command run in $(shell basename ${PWD}):\n"
	@sed -n 's/^##//p' $< | column -t -s ":" |  sed -e 's/^/ /' ; echo

# Place here all the variables that you need
export <++> ?= "default_value"

## unset: remove variables from terminal session `source <(make unset)`
.PHONY: unset
unset:
	@echo 'unset <++>'

## env: print required variables
.PHONY: env
env:
	@echo 'export <++>=${<++>}'

## show-env: Show passed environment to targets
.PHONY: show-env
show-env:
	@env

## setup: Prepare the environment to be used
setup:
	@echo "This is the setup"
	@echo "--------------"
