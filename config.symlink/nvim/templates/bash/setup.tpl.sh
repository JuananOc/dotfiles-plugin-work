#!/usr/bin/env bash

# As first step here we need to execute the following VIM commands to setup the topic name when the topic is called 'name':
# :%s/TOPIC/NAME/g
# :%s/topic/name/g

# ==============================================================
# Main variables and common aliases to enter inside the instance
# ==============================================================

BACKUP_FOLDER="$HOME/Downloads"

TOPIC="$HOME/.dotfiles/plugings/topic-sensitive"
TOPICBIN="$TOPIC/bin"
TOPICCONFIG="$TOPIC/conf"

TOPICSENSITIVE="$HOME/.dotfiles/plugins/topic-sensitive"
TOPICCONFIGSENSITIVE="$HOME/.dotfiles/plugins/topic-sensitive/conf"
TOPICIAC="$HOME/projects/topic/iac"

alias topic="cd $TOPIC"
alias topiciac="cd $TOPICIAC"
alias topicconf="cd $TOPICCONFIG"
alias topicsetup="cd $TOPICCONFIGSENSITIVE"
alias topics="vim $TOPICCONFIG/setup.zsh"

# ================
# Public functions
# ================
# Those functions are known to start with an underscore to specify that they are called from the terminal on demand

# Function to ensure that there is no files required
function topic_backup() {
    repositories_status $TOPICIAC
    backpy__create_backup $BACKUP_FOLDER
}

# Pull changes in current branches of all repositories
function topic_pull_repositories() {
    repositories_update $TOPICIAC
}

# Pull changes in current branches of all repositories
function topic_checkout_repositories() {
    repositories_checkout $TOPICIAC ${1:-master}
}

function topic_create_repositories {
    local root_repo_folder="$TOPICIAC"

    # create_repository $root_repo_folder "internal" "dotfiles" "git@gitlab.com:topic/internal/dotfiles/dotfiles.git"
}

function topic_setup() {
    blue "> Setup topic"

    mkdir -p $TOPIC
    mkdir -p $TOPICBIN
    mkdir -p $TOPICCONFIG
    mkdir -p $TOPICIAC

    _topic_enable_path
    _topic_create_ssh_keys
    _topic_generate_common_files
    _topic_git_setup

    # Additional setup steps
    topic_create_repositories
}

function ic_browser() {
    # This functions requires Firefox and also the plugin "open external links in a container"
    FIREFOX_BIN="/Applications/Firefox.app/Contents/MacOS/firefox"
    TOPIC_CONTAINER="ext+container:name=topic"
    PINNED="pinned=true"
    URLS=('https://duckduckgo.com' 'https://duckduckgo.com')

    $FIREFOX_BIN --window --window-size 20

    for url in $URLS; do
      $FIREFOX_BIN "${TOPIC_CONTAINER}&url=${url}&${PINNED}"
    done
}


# ================
# Helper functions
# ================
# Those functions are know to start with an underscore to specify that they are not intended to be called directly from shell

function _topic_enable_path() {
    blue "> Add topic binaries to the path to have the highest precedence"
    export PATH="$TOPICBIN:$PATH"
}

function _topic_create_ssh_keys() {
    local ssh_key_name="topic_id_rsa"

    if [[ ! -f $TOPICCONFIG/topic_id_rsa ]] && [[ $TOPICCONFIG/topic_id_rsa.pub ]]; then
        ssh-keygen -f $TOPICCONFIG/topic_id_rsa -t rsa -b 4096
        blue "> Ensure to save the paassword for your SSH key in a secure place. Also remember that you need to add it inside your account to be ablle to use it in the client"
    else
        blue "> SSH Keys for Autentia project already exists. Ensure that have correct permissions..."
        chmod 600 $TOPICCONFIG/topic_id_rsa
        chmod 600 $TOPICCONFIG/topic_id_rsa.pub
    fi
}

function _topic_generate_common_files() {
    create_aws_configuration
    create_aws_credentials
}

function _topic_git_setup() {
    if [[ ! -f $TOPICCONFIGSENSITIVE/ssh-config ]]; then
      blue 'Ensure SSH configuration applied is the custom one'
      git config core.sshCommand "ssh -F $TOPICCONFIGSENSITIVE/ssh-config"
    fi

    blue 'Add to the SSH agent the password'
    eval `ssh-agent -s` ssh-add $TOPICCONFIGSENSITIVE/topic_id_rsa
}



# =============================
# Execution on loading terminal
# =============================
# This section is intended to be executed whenever this setup is loaded entering the Terminal environment
function main() {
  blue 'Welcome to topic shell'
}

main
