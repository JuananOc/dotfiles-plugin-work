" Juanan Neovim configuration

" General configuration {{{
:let mapleader = " "
:let maplocalleader = ","

set encoding=utf-8
set showmode
set confirm
set lazyredraw                       " redraw only when we need to
set scrolloff=15                     " perform scroll maintaining the center of the page
set sidescrolloff=15                 " perform scroll maintaining the center of the page

set nobackup
set nowritebackup
set noswapfile
set noundofile

" Vimrc commands
" Change my vimrc file
:nnoremap <leader>ev :vsplit $MYVIMRC<cr>
" Reload vimrc file
:nnoremap <leader>sv :source $MYVIMRC<cr>:noh<cr>

" UI settings
set ruler
set number
set relativenumber
set list
set listchars=tab:>·,trail:~

" Allow gf to open non-existent files
map gf :edit <cfile><cr>

" Movements utilities
:nnoremap H 0
:nnoremap L $
:vnoremap H 0
:vnoremap L $

" Close buffers
:nnoremap <silent> <leader>x :close<CR>

" Search utilities
set wildmenu                        " show all options when tabbing
set incsearch                       " search as characters are entered
set hlsearch                        " highlight matches
:nnoremap n nzz
:nnoremap N Nzz

" Copy utilities.
:nnoremap Y y$
:nnoremap <leader>y mmV"*y'm
:nnoremap <leader>Y mmggVG"*y'm
:vnoremap <leader>y "*y

" Copy the current filepath in the registers
:nnoremap <C-g> :call CopyFilePath()<CR>
function! CopyFilePath()
  let path = expand('%:p')
  let @" = path
  let @+ = path
  echom path
endfunction

" Copy the current branch in the registers
:nnoremap <leader>cb :call CopyBranchName()<CR>
function! CopyBranchName()
  let branch = system("git branch --show-current 2> /dev/null | tr -d '\n'")
  let @" = branch
  let @+ = branch
  echom branch
endfunction

" Paste utilities.
nmap <leader>p "+p<CR>

" Save/Quit utilities
nmap <leader>w :w<CR>
nmap <leader>q :q<CR>
nmap <leader>wq :wq<CR>
nmap <leader>qa :qa!<CR>

" Tab utilities
set expandtab
set backspace=indent,eol,start      " backspace always
set smarttab                        " tab key in insert mode insert spaces or tabs to go to the next indent of the next tabstop when cursor is at the beggining of a line.
nnoremap <Tab> >l
nnoremap <S-Tab> <h
vnoremap <Tab> >gv
vnoremap <S-Tab> <gv


" create directories if they don't exist
" from https://vi.stackexchange.com/questions/678/how-do-i-save-a-file-in-a-directory-that-does-not-yet-exist
augroup Mkdir
    autocmd!
    autocmd BufWritePre * call mkdir(expand("<afile>:p:h"), "p")
augroup END

" Try to save with sudo command
:cnoremap w!! execute 'silent! write !sudo tee % >/dev/null' <bar> edit!


" Diff open files
nnoremap <leader>dt :windo diffthis<CR>
nnoremap <leader>do :windo diffoff<CR>

" }}}

" Terminal utilities {{{
" Open terminal in current folder
nnoremap <leader>ot <ESC><C-w>s<C-w>j:let $VIM_DIR=expand('%:p:h')<CR>:terminal<CR>Acd $VIM_DIR<CR>
" Open terminal in current folder
nnoremap <leader>ovt <ESC><C-w>v:let $VIM_DIR=expand('%:p:h')<CR>:terminal<CR>Acd $VIM_DIR<CR>
" }}}

" Status line {{{
set laststatus=2                                                                    " show always status bar
set statusline=                                                                     " clear the statusline for when vimrc is reloaded
set statusline+=%f\                                                                 " file name
set statusline+=%h                                                                  " Help file flag
set statusline+=%m                                                                  " Modified flag
set statusline+=%r                                                                  " Read only flag
set statusline+=%=                                                                  " right align
set statusline+=%-16(%{exists('g:loaded_fugitive')?fugitive#statusline():''}\%)     " Git project status
set statusline+=col:\ %c\                                                           " Column number
set statusline+=%l/%L                                                               " Cursor line / total lines
set statusline+=[%{strlen(&fenc)?&fenc:&enc}]                                       " encoding
set statusline+=[%{strlen(&ft)?&ft:'none'}]                                         " filetype
" }}}

" Templating {{{
" Templates with <++> are defined in the specific filetypes, so you will have
" some templates defined there
" Go to next <++> mark.
" "_ makes reference to null register to don´t override what you have yanked
" in your default register
nnoremap <localleader><localleader> <Esc>/<++><Enter>"_c4l
inoremap <localleader><localleader> <Esc>/<++><Enter>"_c4l
" }}}

" Folding {{{
" It's the process of collapsing multiples lines of text into a single line
"
set foldenable                      " enable folding
set foldlevelstart=10               " open most folds by default
set foldnestmax=10                  " Folds can be nested. Set max value of 10 nested fold max
set foldmethod=manual               " Defines the type of folding
set foldcolumn=2
" }}}

" Plugins {{{
" Automatically install vim-plug and run PlugInstall if vim-plug not found
if empty(glob('~/.config/nvim/autoload/plug.vim'))
    silent !curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs
                \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    autocmd VimEnter * PlugInstall | source $MYVIMRC
endif

call plug#begin('~/.config/nvim/plugged')

call system('mkdir ~/.config/nvim/plugs')

" source ~/.config/nvim/plugs/ansible.vim
source ~/.config/nvim/plugs/editorconfig.vim
source ~/.config/nvim/plugs/harpoon.vim
source ~/.config/nvim/plugs/ide.vim
" source ~/.config/nvim/plugs/nord.vim
source ~/.config/nvim/plugs/nvim-web-devicons.vim
source ~/.config/nvim/plugs/nvim-telescope.vim
source ~/.config/nvim/plugs/nvim-treesitter.vim
source ~/.config/nvim/plugs/onenord.vim
source ~/.config/nvim/plugs/plenary.vim
source ~/.config/nvim/plugs/project.vim
source ~/.config/nvim/plugs/toggleterm.vim
" source ~/.config/nvim/plugs/vim-bookmarks.vim
" source ~/.config/nvim/plugs/vim-bufsurf.vim
source ~/.config/nvim/plugs/vim-fugitive.vim

call plug#end()

" colorscheme nord
colorscheme onenord

call system('mkdir ~/.config/nvim/after/plugin')
" source ~/.config/nvim/after/plugin/ansible.vim
luafile ~/.config/nvim/after/plugin/cmp.lua
luafile ~/.config/nvim/after/plugin/lspconfig.lua
luafile ~/.config/nvim/after/plugin/lspkind.lua
luafile ~/.config/nvim/after/plugin/lspsaga.lua
luafile ~/.config/nvim/after/plugin/nvim-web-devicons.lua
luafile ~/.config/nvim/after/plugin/project.lua
luafile ~/.config/nvim/after/plugin/telescope.lua
luafile ~/.config/nvim/after/plugin/toggleterm.lua
luafile ~/.config/nvim/after/plugin/treesitter.lua
luafile ~/.config/nvim/after/plugin/onenord.lua
" }}}

" vim:foldmethod=marker:foldlevel=0
