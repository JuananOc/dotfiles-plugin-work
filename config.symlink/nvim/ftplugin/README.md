The autocmd hooks that set a buffer’s filetype are defined in $VIMRUNTIME/filetype.vim.
They apply heuristics to guess and then set the type of a buffer, and then Vim runs any appropriate filetype plugins afterwards: files in 'runtimepath' directories named ftplugin/FILETYPE.vim will be sourced.

So creating in this directory .vim/ftplugin/<filetype>.vim files will apply this commands for the specific files.
If this settings are growing a lot (for example in spells and templates), you can create those structure too and will be automatically read:

```shell
* .vim/ftplugin/<filetype>/spell.vim
* .vim/ftplugin/<filetype>/template.vim
```

or

```shell
* .vim/ftplugin/<filetype>_spell.vim
* .vim/ftplugin/<filetype>_template.vim
```

NOTE: Here the path .vim/ are not followed by after because it's related only with ftplugin section.
