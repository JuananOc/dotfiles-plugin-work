" Add comments
nnoremap <buffer> <localleader>c I#<space><esc>

" Bash code
nnoremap <localleader>v <Esc>:-1read $HOME/.config/nvim/templates/terraform/variables.tpl.tf
inoremap <localleader>v <Esc>:-1read $HOME/.config/nvim/templates/terraform/variables.tpl.tf

nnoremap <localleader>o <Esc>:-1read $HOME/.config/nvim/templates/terraform/outputs.tpl.tf
inoremap <localleader>o <Esc>:-1read $HOME/.config/nvim/templates/terraform/outputs.tpl.tf
