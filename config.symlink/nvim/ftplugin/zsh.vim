" Comments
nnoremap <buffer> <localleader>c I#<space><esc>

" Bash code
inoremap <localleader>stp <Esc>:-1read $HOME/.config/nvim/templates/bash/setup.tpl.sh

