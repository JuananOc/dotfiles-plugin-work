Plug 'tpope/vim-fugitive'

nnoremap <Leader>g :Git<CR>
nnoremap <Leader>gb :Git blame<CR>
nnoremap <Leader>gP :Git push<CR>
" Open the current file log history in the quickfix list
nnoremap <Leader>gl :Gclog %<CR>
" Open log history in the quickfix list
nnoremap <Leader>gL :Gclog<CR>
