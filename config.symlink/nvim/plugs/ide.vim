" Language server provider official neovim plugin
Plug 'neovim/nvim-lspconfig'

" Completions core and from different vim utilities
Plug 'hrsh7th/cmp-nvim-lsp' " Pending to see if is usable.
Plug 'hrsh7th/cmp-buffer'
Plug 'hrsh7th/cmp-path'
Plug 'hrsh7th/cmp-cmdline'
Plug 'hrsh7th/nvim-cmp'

" For luasnip users.
Plug 'L3MON4D3/LuaSnip'
Plug 'saadparwaiz1/cmp_luasnip'

" To show snippets types
Plug 'onsails/lspkind-nvim'

" A light-weight lsp plugin based on neovim built-in lsp with highly a performant UI
source ~/.config/nvim/plugs/nvim-lspsaga.vim

set completeopt=menuone,noselect,noinsert
