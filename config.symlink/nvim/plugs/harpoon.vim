Plug 'nvim-lua/plenary.nvim' " don't forget to add this one if you don't have it yet!
Plug 'ThePrimeagen/harpoon'

:nnoremap <leader>h :lua require("harpoon.mark").add_file()<cr>
:nnoremap <leader>hl :lua require("harpoon.ui").toggle_quick_menu()<cr>
