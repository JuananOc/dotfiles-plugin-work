Plug 'akinsho/toggleterm.nvim', {'tag' : '*'}

" Open toggle terminals
" =====================
" set
autocmd TermEnter term://*toggleterm#*
      \ tnoremap <silent><c-t> <Cmd>exe v:count1 . "ToggleTerm"<CR>

" By applying the mappings this way you can pass a count to your
" mapping to open a specific window.
" For example: 2<C-t> will open terminal 2
nnoremap <silent><C-t> <Cmd>exe v:count1 . "ToggleTerm"<CR>
inoremap <silent><C-t> <Esc><Cmd>exe v:count1 . "ToggleTerm"<CR>

nnoremap <silent><C-T> <Cmd>exe v:count1 . "ToggleTerm"<CR>
inoremap <silent><C-T> <Esc><Cmd>exe v:count1 . "ToggleTerm"<CR>

nnoremap <leader>sl <Cmd>exe "ToggleTermSendCurrentLine"<CR>
nnoremap <leader>tn <Cmd>exe "ToggleTermSetName"<CR>
