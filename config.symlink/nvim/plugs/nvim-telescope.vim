Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-telescope/telescope.nvim'

" Show all files
nnoremap <leader>ff <cmd>Telescope find_files<cr>
" Show old files opened
nnoremap <leader>fh <cmd>Telescope oldfiles<cr>

" Show current file lines
nnoremap <leader>l <cmd>Telescope current_buffer_fuzzy_find<cr>

" Find across all lines from the folder
nnoremap <leader>gg <cmd>Telescope live_grep<cr>

" Show git utilities
nnoremap <leader>gB <cmd>Telescope git_branches<cr>
" nnoremap <leader>fc <cmd>Telescope git_commits<cr>
nnoremap <leader>gs <cmd>Telescope git_stash<cr>

" Show treesitter variables, functions, classes detected
nnoremap <leader>fn <cmd>Telescope treesitter<cr>

" Show all the vim commands available
nnoremap <leader>fc <cmd>Telescope commands<cr>

" Show tags created by Vim
nnoremap <leader>ft <cmd>Telescope help_tags<cr>

" Show vim buffer files
nnoremap <leader>b <cmd>Telescope buffers<cr>
"
" Show projects from vim projects plugin
nnoremap <leader>fp <cmd>Telescope projects<cr>
