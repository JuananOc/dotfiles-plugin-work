Plug 'pearofducks/ansible-vim', { 'do': './UltiSnips/generate.sh' }


" Watch options settings inside: https://github.com/pearofducks/ansible-vim
let g:ansible_unindent_after_newline = 1
let g:ansible_name_highlight = 'b'
let g:ansible_extra_keywords_highlight = 1
let g:ansible_template_syntaxes = { '*.rb.j2': 'ruby' }
