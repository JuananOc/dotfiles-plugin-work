function finddir() {
    find . -type d -name "$1"
}

function findfile() { 
    find . -type f -name "$1"
}

function grep_filtered(){
    grep --exclude-dir=.terragrunt-cache -i "$1" -R ** 
}

function clear_screen_and_clear_tmux_history() {
    tmux clear-history
    clear
}

function loop(){
    while true;
    do
        $* && sleep 5
    done;
}

function madrid(){
    curl -4 'http://wttr.in/madrid?m'
}

function santander(){
    curl -4 'http://wttr.in/santander?m'
}

# This function makes a diff between a private key an a public key and show output.
# If there is any difference if may show any output
function diff-rsa-keys(){
  PRIVKEY=$1
  PUBLICKEY=$2

  if [[ $# -ne 2 ]];then
    blue "Usage: diff-rsa-keys PRIVATE-KEY-PATH PUBLIC-KEY-PATH"
  fi

  diff <( ssh-keygen -y -e -f "$PRIVKEY" ) <( ssh-keygen -y -e -f "$PUBLICKEY" )
}
