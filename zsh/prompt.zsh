# Comment explaining the behavior related to pressing Enter without executing a command
# When you press Enter without executing a command, the precmd hook is not automatically triggered.
# Therefore, the end time is not updated, and the command duration is not calculated.
# To manually update the prompt after pressing Enter, you can type 'precmd' and press Enter.
# This will force the precmd function to execute, updating the end time and refreshing the prompt.

# Setup configuration to autoload
autoload colors && colors
autoload -U add-zsh-hook

# ----------------
# Global Variables
# ----------------

# Color
# -----
PR_NO_COLOR="%{$reset_color%}"
PR_WHITE="%{$fg[white]%}"
PR_GREY="%{$fg[grey]%}"
PR_CYAN="%{$fg[cyan]%}"
PR_BLUE="%{$fg[blue]%}"
PR_RED="%{$fg[red]%}"
PR_YELLOW="%{$fg[yellow]%}"
PR_GREEN="%{$fg[green]%}"
PR_MAGENTA="%{$fg[magenta]%}"

# Special Characters
# ------------------
# Use extended characters to look nicer if supported.
if [[ "${langinfo[CODESET]}" = UTF-8 ]]; then
  PR_SET_CHARSET=""
  PR_HBAR="─"
  PR_ULCORNER="┌"
  PR_LLCORNER="└"
  PR_LRCORNER="┘"
  PR_URCORNER="┐"
else
  typeset -g -A altchar
  set -A altchar ${(s..)terminfo[acsc]}
  # Some stuff to help us draw nice lines
  PR_SET_CHARSET="%{$terminfo[enacs]%}"
  PR_SHIFT_IN="%{$terminfo[smacs]%}"
  PR_SHIFT_OUT="%{$terminfo[rmacs]%}"
  PR_HBAR="${PR_SHIFT_IN}${altchar[q]:--}${PR_SHIFT_OUT}"
  PR_ULCORNER="${PR_SHIFT_IN}${altchar[l]:--}${PR_SHIFT_OUT}"
  PR_LLCORNER="${PR_SHIFT_IN}${altchar[m]:--}${PR_SHIFT_OUT}"
  PR_LRCORNER="${PR_SHIFT_IN}${altchar[j]:--}${PR_SHIFT_OUT}"
  PR_URCORNER="${PR_SHIFT_IN}${altchar[k]:--}${PR_SHIFT_OUT}"
fi

# -------------------
# Dynamic information
# -------------------
# This block contains all the funcions that needs to be updated before every prompt is showed.
# If we want to add new dynamic inforamtion into the prompt we need to:
# - Create a new function here.
# - Use it inside `update_prompt` function
# - Update the prompt_str from theme_precmd to size the horizontal bars accordingly.

# Git
# ---
get_git_info() {
    local git_dir="$(git rev-parse --git-dir 2>/dev/null)"

    if [[ -n "$git_dir" ]]; then
        local git_branch="${$(git rev-parse --abbrev-ref HEAD 2>/dev/null):-N/A}"
        local git_is_behind_remote_cmd_output=$(git rev-list --count --left-only '@{upstream}...HEAD' 2>/dev/null)
        local pull_required=$([[ $git_is_behind_remote_cmd_output -gt 0 ]] && echo "(pull required)" || echo "")
        echo "[${git_branch}${pull_required}]"
    else
        echo ""
    fi
}

# AWS
# ---
get_aws_info() {
    if [[ -n $AWS_PROFILE ]]; then
      echo "[$AWS_PROFILE]"
    else
      echo
    fi
}

# Date
# ----
# We use this instead of ZSH special variables because this way we use the same method to print the PROMPT and calculate the lines required.
get_date_time() {
    echo "[$(date '+%Y-%m-%d %H:%M')]"
}

# Elaspsed time
# -------------
get_last_command_elapsed_time() {
    # SECONDS is a built-in special variable that keeps track of the total number of seconds the shell has been running.
    # It's automatically accessible in all functions without being declared as global.

    # Internal function to convert seconds to days, hours, minutes, and seconds
    seconds2days() {
        local days=$((($1/60)/60/24))
        local hours=$((($1/60)/60%24))
        local minutes=$((($1/60)%60))
        local seconds=$(($1%60))

        printf "%ddays,%02d:%02d:%02d" $days $hours $minutes $seconds |
        sed 's/^1days/1day/;s/^0days,\(00:\)*//;s/^0//' ;
    }

    local current_duration=$((SECONDS - start_time))
    local elapsed_time=$(seconds2days $current_duration)
    print "[elapsed time:${elapsed_time}]"
}

# PWD
# ----
# We use this instead of ZSH special variables because this way we use the same method to print the PROMPT and calculate the lines required.
get_pwd() {
  pwd
}

# K8s
# ---
# Function to show the current context being used in the terminal
get_k8s_info() {
    local k8s_context="$(kccc 2&>/dev/null)"
    if [[ -n $k8s_context ]]; then
      echo "[$k8s_context]" 
    else
      echo
    fi
}

# ----------------
# ZSH custom hooks
# ----------------
add-zsh-hook precmd  theme_precmd

# This function uses the first line prompt to calculate the missing space in the terminal in order to create missing horizontal bars.
# The most important part here is the variable $prompt_str, which it's the first line that we define in our PROMPT variable so we ensure it's sized properly.
# Changes in the prompt first line should also be updated here.
function theme_precmd {
  local TERMWIDTH=$(( COLUMNS - ${ZLE_RPROMPT_INDENT:-1} ))

  PR_FILLBAR=""
  PR_PWDLEN=""

  local prompt_str=${PR_ULCORNER}${PR_HBAR}$(get_date_time)${PR_HBAR}$(get_pwd)${(e)PR_FILLBAR}$(get_aws_info)${PR_HBAR}$(get_k8s_info)${PR_HBAR}$(get_git_info)${PR_URCORNER}
  local promptsize=${#${(%):-${prompt_str}}}

  # Truncate the path if it's too long.
  if (( promptsize > TERMWIDTH )); then
    (( PR_PWDLEN = TERMWIDTH ))
  elif [[ "${langinfo[CODESET]}" = UTF-8 ]]; then
    PR_FILLBAR="\${(l:$(( TERMWIDTH - promptsize ))::${PR_HBAR}:)}"
  else
    PR_FILLBAR="${PR_SHIFT_IN}\${(l:$(( TERMWIDTH - promptsize ))::${altchar[q]:--}:)}${PR_SHIFT_OUT}"
  fi

  # For debugging purposes
  # echo "PROMPT_STR:\n$prompt_str"
  # echo "PROMPTSIZE:\n$promptsize"
}

# Hook to run after each command
precmd() {
    # Update end_time after each command
    end_time=$SECONDS
    update_prompt
}

# Hook to run before each command
preexec() {
    # Capture the start time for each command
    start_time=$SECONDS
}

# Function to set RPROMPT with AWS, k8s, git information, and command duration
update_prompt () {
    # Prompt variable content
    local whoami="$(whoami)"
    local command_return_code="%? ↵ "
    local command_elapsed_time="$(get_last_command_elapsed_time)"

    # _prompt variables are the previous ones with the formatting to show in the PROMPT
    local date_time_prompt="${PR_YELLOW}$(get_date_time)${PR_NO_COLOR}"
    local directory_prompt="${PR_CYAN}$(get_pwd)${PR_NO_COLOR}"
    local aws_info_prompt="${PR_RED}$(get_aws_info)${PR_NO_COLOR}"
    local k8s_info_prompt="${PR_BLUE}$(get_k8s_info)${PR_NO_COLOR}"
    local git_info_prompt="${PR_MAGENTA}$(get_git_info)${PR_NO_COLOR}"

    local whoami_prompt="${PR_CYAN}$whoami${PR_NO_COLOR}"
    local initial_chars_propmt="${PR_RED}>${PR_GREEN}>${PR_CYAN}>${PR_NO_COLOR} ${PR_WHITE}%(!.#.%%)${PR_NO_COLOR} "
    local return_code_prompt="${PR_RED}$command_return_code${PR_NO_COLOR}"
    local elapsed_time_prompt="${PR_GREEN}$command_elapsed_time${PR_NO_COLOR}"

    export PROMPT="${PR_ULCORNER}${PR_HBAR}$date_time_prompt${PR_HBAR}%${PR_PWDLEN}<...<${directory_prompt}%<<${(e)PR_FILLBAR}$aws_info_prompt${PR_HBAR}$k8s_info_prompt${PR_HBAR}$git_info_prompt${PR_URCORNER}
${PR_LLCORNER}$whoami_prompt${PR_HBAR}$initial_chars_propmt"

    # Calculate the duration only if start_time is defined (i.e., a command has been executed)
    if [[ -n $start_time ]]; then
        local rprompt="$return_code_prompt${PR_HBAR}$elapsed_time_prompt${PR_LRCORNER}"
        RPROMPT=$rprompt
    else
        # No command has been executed, so reset RPROMPT
        RPROMPT=""
    fi
}


# ----
# Main
# ----
# Initial call to update_prompt to populate PROMPT
update_prompt

# Initialize SECONDS to capture the start time when the shell starts. This command should be the last so any second is lost
SECONDS=0
