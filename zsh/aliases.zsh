# GENERAL
alias ..='cd ../'
alias ...='cd ../../'
alias ....='cd ../../../'

alias l='ls -laGFhS'
alias ls='ls -aGFhS'
alias lg='l | grep --color=auto'

# Go to Dotfiles
alias df="cd ~/.dotfiles; vim ./"
alias pf="cd ~/.dotfiles/plugins/;"
alias dfp="cd ~/.dotfiles/plugins/dotfiles-plugin-personal/; vim ./"

# GENERAL
alias tailf='tail -f'
alias cls='clear'

# FUNCTIONS
alias ff='findfile'
# alias fd='finddir'
alias s='grep_filtered'

# EDITOR
alias vim='nvim'
alias vi='nvim'

# UPDATE PROFILE
alias zp='. ~/.zshrc'

