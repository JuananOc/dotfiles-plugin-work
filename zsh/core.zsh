#!/usr/bin/env bash

# All functions located here are core for the correct work of dotfiles.
# Even when they are used in more specific proyects all of them are stored here for ensuring that another plugins will work.

# Summary: Search for all SOURCE_FILE inside SOURCE_FOLDER and generates the DESTINATION_FILE
function merge_files() {
  local SOURCE_FILE=$1
  local SOURCE_FOLDER=$2
  local DESTINATION_FILE=$3

  blue "Cleanup $DESTINATION_FILE"
  rm $DESTINATION_FILE

  blue "Search for $SOURCE_FILE inside $SOURCE_FOLDER"
  if [[ '' != $(find -H "$SOURCE_FOLDER" -type f -name "$SOURCE_FILE") ]];then
    cat $(find -H "$SOURCE_FOLDER" -type f -name "$SOURCE_FILE") >> $DESTINATION_FILE
  fi

  green "Generated $DESTINATION_FILE"
}

function create_aws_credentials() {
  mkdir -p "$HOME/.aws"
  merge_files "aws-credentials" "$DOTFILES_DIR" "$HOME/.aws/credentials"
  merge_files "aws-mfa-credentials" "$DOTFILES_DIR" "$HOME/.aws/mfa_credentials"
}

function create_aws_configuration() {
  mkdir -p "$HOME/.aws"
  merge_files "aws-config" "$DOTFILES_DIR" "$HOME/.aws/config"
}

function cd() {
  if [ $# -eq 0 ] ; then
    # no arguments
    builtin cd
  elif [ -d $1 ] ; then
    # argument is a directory
    builtin cd "$1"
  else
    # argument is not a directory
    builtin cd "$(dirname $1)"
  fi
}

