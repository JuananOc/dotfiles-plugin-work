# To find also the hidden files. This should be used when usign the vim fzf plugin with <leader>F
export FZF_DEFAULT_COMMAND="find . -path '*/\.*' -type d -prune -o -type f -print -o -type l -print 2> /dev/null | sed s/^..//"
export FZF_DEFAULT_OPTS="--preview-window=down:25% --height=~30% --border --layout reverse --sort --preview 'cat {}'"
