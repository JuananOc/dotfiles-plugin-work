#!/usr/bin/env bash

# In order to be able to automatically load all this kubeconfig files, you need to specify to kubectl to find them.
# This is accomplished via the usage of the KUBECONFIG environment variable, which can contain multiple kubeconfig files separated by `:`. 
# You can see more information on the usage of the KUBECONFIG environment variable in https://kubernetes.io/docs/tasks/access-application-cluster/configure-access-multiple-clusters/#set-the-kubeconfig-environment-variable.
# 
# By manually configuring the first kubeconfig, we can treat it as the ‘main’ kubeconfig from where to get the current context, appending later all the rest of kubeconfigs located in the folder or subfolreds by using the find command. After setting this variable and loading it, you’ll be able to see all the context:
# We can automate the creation of this environment variable in our .zshrc
if [[ -d $HOME/.kube/kubeconfigs ]]; then
  export KUBECONFIG=$HOME/.kube/kubeconfigs/config:$(find $HOME/.kube/kubeconfigs -type f | tr '\n' ':')
fi
