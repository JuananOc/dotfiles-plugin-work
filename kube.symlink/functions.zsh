function import_kubeconfigs() {
  # The DOTFILES_ROOT is always set
  local kubeconfig_src_dirpath="$DOTFILES_ROOT/plugins"
  local kubeconfig_src_dirname="kubeconfigs"
  local kubeconfig_dest_dirpath=$HOME/.kube/kubeconfigs

  blue "Ensure $kubeconfig_dest_dirpath is clean"
  rm -r $kubeconfig_dest_dirpath
  mkdir -p $kubeconfig_dest_dirpath

  blue "Find all folders inside '$kubeconfig_src_dirname' folder and add it inside the kubeconfigs"
  for kubeconfig_dirpath in $(find $kubeconfig_src_dirpath -type d -name $kubeconfig_src_dirname -not -path "*/kube.symlink/*"); do
    kubeconfig_custom_parent_dirname="$(basename $(cd ${kubeconfig_dirpath}/../../ && pwd))"

    blue "> Ensure $kubeconfig_dest_dirpath exists"
    mkdir -p $kubeconfig_dest_dirpath/$kubeconfig_custom_parent_dirname

    blue "> Add kubeconfig folder inside $kubeconfig_dest_dirpath"
    cp -R $kubeconfig_dirpath $kubeconfig_dest_dirpath/$kubeconfig_custom_parent_dirname
  done
}
