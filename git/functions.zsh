#!/usr/bin/env bash

# Colors definition
BLACK="$(tput setaf 0)"
RED="$(tput setaf 1)"
GREEN="$(tput setaf 2)"
YELLOW="$(tput setaf 3)"
BLUE="$(tput setaf 4)"
MAGENTA="$(tput setaf 5)"
CYAN="$(tput setaf 6)"
WHITE="$(tput setaf 7)"
NOCOLOR="$(tput sgr0)"
BOLD="$(tput bold)"
UNDERLINE="$(tput smul)"
NOUNDERLINE="$(tput rmul)"

function red() { echo -e "${RED}${*}${NOCOLOR}"; }
function green() { echo -e "${GREEN}${*}${NOCOLOR}"; }
function yellow() { echo -e "${YELLOW}${*}${NOCOLOR}"; }
function cyan() { echo -e "${CYAN}${*}${NOCOLOR}"; }
function title() { echo -e "${BOLD}${UNDERLINE}${*}${NOUNDERLINE}${NOCOLOR}"; }

# this function create a repository and receive following parameters:
# BASE_DIR: Directory where repository will be created
# PROJECT_DIR: Name of the project where the repository belongs
# REPO_DIR: Name of the directory to perform the repository
# REPO_URL: Repository url to perform the git clone
# DEFAULT_BRANCH: The branch to update the repository
function create_repository() {
    local BASE_DIR=$1
    local PROJECT_DIR=$2
    local REPO_DIR=$3
    local REPO_URL=$4
    local DEFAULT_BRANCH=${5-'master'}

    local REPO_PATH="$BASE_DIR/$PROJECT_DIR/$REPO_DIR"

    if [[ ! -d $REPO_PATH ]]; then
        echo -n -e "    Creating ${CYAN}${REPO_DIR}${NOCOLOR} repo..."
        git clone --quiet "${REPO_URL}" "${REPO_PATH}"
        green "[OK]"
    else
        echo -n -e "    Repository ${CYAN}${REPO_DIR}${NOCOLOR} found. Checking out to ${CYAN}${DEFAULT_BRANCH}${NOCOLOR} and pulling latest changes..."
        cd "${REPO_PATH}"
        if [[ $(git status --porcelain) ]]; then
            red "[FAIL] Local changes detected"
            failed=true
        else
            git checkout --quiet "${DEFAULT_BRANCH}"
            git pull --quiet origin "${DEFAULT_BRANCH}"
            git checkout --quiet -
            green "[OK]"
        fi
        cd - >/dev/null
    fi
}

function repositories_status() {
    local BASE_DIR=$1

    # 1. Check if directory listed have ".git directory
    # 2. Print the repository name
    # 4. Execute git status command (coloured)
    find $BASE_DIR -type d -name ".git" -maxdepth 3 -exec blue "Repository: {}" \; -execdir git status \;
}

function repositories_update() {
    local BASE_DIR=$1

    # 1. Check if directory listed have ".git directory
    # 2. Print the repository name
    # 4. Execute git pull command (coloured)
    find $BASE_DIR -type d -name ".git" -maxdepth 3 -exec blue "Repository: {}" \; -execdir git pull \;
}

function repositories_checkout() {
    local BASE_DIR=$1
    local BRANCH_TO_CHECKOUT=$2

    # 1. Check if directory listed have ".git directory
    # 2. Print the repository name
    # 4. Execute git checkout master command (coloured)
    find $BASE_DIR -type d -name ".git" -maxdepth 3 -exec blue "Repository: {}" \; -execdir git checkout $BRANCH_TO_CHECKOUT \;
}

# Get the .editconfig inside dotfiles and copy to the current folder
function repository_setup() {
  blue "Creating .editorconfig file"
  cp $HOME/.dotfiles/plugins/dotfiles-plugin-personal/.editorconfig .
}


# Prune the branches in sync with the remote (but master, development, integration preproduction and production) and delete trash branches.
# If the branch is not in sync it won't be deleted
function clean_branches() {
  blue "Cleaning unused branches"
  git branch --format '%(refname:short)' | grep -v -E '^(master|main|development|integration|production)$' | xargs git branch -d && git fetch --prune
}

# Create an empty commit with default message
# usage: clean_branches PLATEU-827 [commit message for something]
function git_empty_commit() {
  $TASK=$1
  $MESSAGE=${2:-"dummy commit"}
  git commit --allow-empty -m "chore($TASK): $MESSAGE"
}
