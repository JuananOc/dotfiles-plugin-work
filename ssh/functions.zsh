#!/usr/bin/env bash

function create_ssh_configuration() {
  mkdir -p "$HOME/.ssh"
  merge_files "ssh-config" "$DOTFILES_DIR" "$HOME/.ssh/config"
}
